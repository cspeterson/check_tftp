""" End-to-end tests """
import hashlib
import multiprocessing
import os
import socket
import subprocess
import tempfile

import pytest  # type:ignore
import tftpy  # type:ignore


def unused_port() -> int:
    """Get an unused port that we can ignore for testing failure"""
    sock = socket.socket()
    sock.bind(("", 0))
    return sock.getsockname()[1]


SERVER_PORT = unused_port()
FILENAME = "file.txt"
FILE_CONTENTS = "hello"
BLACKHOLE_PORT = 9970


@pytest.fixture(scope="session")
def tftp_server():
    """
    Serve tftp from a temp directory containing one file: `FILENAME` with the
    contents `hello`.
    """
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpfile = os.path.join(tmpdir, FILENAME)
        with open(tmpfile, "w", encoding="utf-8") as file:
            file.write(FILE_CONTENTS)
        server = tftpy.TftpServer(tmpdir)
        proc = multiprocessing.Process(
            target=server.listen, args=("localhost", SERVER_PORT)
        )
        proc.start()
        yield True
        proc.terminate()


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (
            # OK
            [
                "--hostname",
                "localhost",
                "--port",
                SERVER_PORT,
                FILENAME,
            ],
            {
                "returncode": 0,
                "output_contains": "TFTPREQ OK",
            },
        ),
        (
            # OK, check regex pattern
            [
                "--hostname",
                "localhost",
                "--port",
                SERVER_PORT,
                "--pattern",
                FILE_CONTENTS,
                FILENAME,
            ],
            {
                "returncode": 0,
                "output_contains": "TFTPREQ OK",
            },
        ),
        (
            # OK, check checksum
            [
                "--hostname",
                "localhost",
                "--port",
                SERVER_PORT,
                "--checksum",
                hashlib.md5(FILE_CONTENTS.encode("utf-8")).hexdigest(),
                FILENAME,
            ],
            {
                "returncode": 0,
                "output_contains": "TFTPREQ OK",
            },
        ),
        (
            # WARN re download duration
            [
                "--hostname",
                "localhost",
                "--port",
                SERVER_PORT,
                "--warning",
                "60:",  # will fail if it takes *LESS* than 60s to download
                FILENAME,
            ],
            {
                "returncode": 1,
                "output_contains": "TFTPREQ WARNING",
            },
        ),
        (
            # CRIT re download duration
            [
                "--hostname",
                "localhost",
                "--port",
                SERVER_PORT,
                "--critical",
                "60:",  # will fail if it takes *LESS* than 60s to download
                FILENAME,
            ],
            {
                "returncode": 2,
                "output_contains": "TFTPREQ CRITICAL",
            },
        ),
        (
            # Unkown, fail on timeout by going to a port that isn't listening
            [
                "--hostname",
                "localhost",
                "--port",
                unused_port(),
                "--timeout",
                1,
                FILENAME,
            ],
            {
                "returncode": 3,
                "output_contains": "TFTPREQ UNKNOWN: Exception: Timed-out",
            },
        ),
    ],
)
# pylint: disable=unused-argument
# pylint: disable=redefined-outer-name
def test_end_to_end(tftp_server, test_input, expected):
    """Test"""
    command = ["python3", "-m", "check_tftp"] + [str(x) for x in test_input]
    res = subprocess.run(command, capture_output=True, check=False, text=True)
    assert res.returncode == expected["returncode"]
    assert expected["output_contains"] in res.stdout


# pylint: enable=unused-argument
# pylint: enable=redefined-outer-name
